# OpenML dataset: Marginal-Revolution-Blog-Post-Data

https://www.openml.org/d/43574

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The following dataset contains data on blog posts from MarginalRevolution.com. For posts from Jan. 1, 2010 to 9/17/2016, the following attributes are gathered.

Author Name
Post Title 
Post Date
Post content (words)
Number of Words in post
Number of Comments in post
Dummy variable for several commonly used categories

The data was scraped using Python's Beautiful Soup package, and cleaned in R. See my github page (https://github.com/wnowak10/) for the Python and R code.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43574) of an [OpenML dataset](https://www.openml.org/d/43574). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43574/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43574/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43574/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

